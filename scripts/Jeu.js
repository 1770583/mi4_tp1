export default class Jeu {

  constructor() {

    console.log("Création de la classe Jeu");

    // Référence au canevas dans le DOM
    this.canvas = document.querySelector("canvas");

    // Paramètres modifiable du jeu
    this.params = {
      nombreBlocs: 3,
      tailleBloc: 125,
      cadence: 60
    };

    this.initialiser();
    this.debuter();

  }

  initialiser() {

    // Ajustement du canevas à la taille de la fenêtre
    this.canvas.width = document.documentElement.clientWidth;
    this.canvas.height = document.documentElement.clientHeight;

    // Préparation du StageGL
    this.stage = new createjs.StageGL(this.canvas);
    this.stage.setClearColor("black");

    // Préparation du Ticker
    createjs.Ticker.addEventListener("tick", e => this.stage.update(e));
    createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
    createjs.Ticker.framerate = this.params.cadence;

    // Ajout d'un écouteur pour la détection de collisions
    createjs.Ticker.addEventListener("tick", this.detecter.bind(this));

  }

  debuter() {



  }

  detecter() {

    // Filtrage de la liste d'affichage pour ne retenir que les éléments qui nous intéressent
    let projectiles = this.stage.children.filter(item => item instanceof Projectile);
    let blocs = this.stage.children.filter(item => item instanceof Bloc);

    // console.log(blocs.length);

    projectiles.forEach(projectile => {

      // Vérification d'une collision entre un projectile et un bloc
      blocs.forEach(bloc => {

        let p = bloc.globalToLocal(projectile.x, projectile.y);

        if (bloc.hitTest(p.x, p.y)) {
          console.log("Boom!");
          projectile.detruire();
          bloc.detruire();
        }

      });

    });

  }

}
